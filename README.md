# BB8's chassis controller

A ROS node posing an interface to BB8's various actuators and sensors,
all in one place.

## Subscriptions

* geometry\_msgs/Twist: Motor commands


## Publications

* sensor\_msgs/battery\_state: Battery State
* raspi\_i2c\_controller/I2CMessage: motor commands

## Links

* [Project Wiki](https://bitbucket.org/cre8bb8/doc/wiki/Home)

