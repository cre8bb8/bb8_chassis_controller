#ifndef BB8_CHASSIS_CONTROLLER_UTILS_H
#define BB8_CHASSIS_CONTROLLER_UTILS_H

template <typename T> T clamp(T min, T max, T val) {
    T tmp = val > min ? val : min;
    return tmp < max ? tmp : max;
}


#endif
