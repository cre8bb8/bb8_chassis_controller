#include <ros/ros.h>
#include "BB8ChassisController.h"

int main( int argc, char** argv )
{
    ros::init(argc, argv, "bb8_chassis_controller");
    ros::NodeHandle nh;
    ros::NodeHandle nh_private("~");

    bb8_chassis_controller::BB8ChassisController BB8ChassisController(nh, nh_private);

    ros::spin();

    return 0;
}

