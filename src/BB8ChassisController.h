#ifndef BB8_CHASSIS_CONTROLLER_BB8_CHASSIS_CONTROLLER_H
#define BB8_CHASSIS_CONTROLLER_BB8_CHASSIS_CONTROLLER_H

#include <ros/ros.h>

#include "geometry_msgs/Twist.h"

namespace bb8_chassis_controller{

    class BB8ChassisController {
        public:
            BB8ChassisController(ros::NodeHandle& nh, ros::NodeHandle& nh_private);

        private:
            ros::NodeHandle nh_;
            ros::NodeHandle nh_private_;

            // battery monitoring
            ros::Publisher  batteryStatePublisher_;
            std::string batteryStateTopic_;
            void publishBatteryStateTimed(const ros::TimerEvent& timerEvent);

            // stuf for controlling actuators
            ros::Subscriber twistSubscriber_;
            std::string twistTopic_;
            void twistCallback(const geometry_msgs::Twist& twistMsg);

            // access to I2C bus
            ros::Publisher  I2CPublisher_;
            ros::ServiceClient I2CClient_;
            std::string I2CMessageTopic_;
            std::string I2CBusServiceName_;

    };
} // Namespace

#endif // include guard

