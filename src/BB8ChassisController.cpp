#include "BB8ChassisController.h"

#include <cmath>
#include <cstdint>

#include <ros/ros.h>
#include "geometry_msgs/Twist.h"
#include "sensor_msgs/BatteryState.h"
#include "raspi_i2c_buscontroller/I2CBusAccess.h"
#include "raspi_i2c_buscontroller/I2CMessage.h"

#include "utils.h"

namespace bb8_chassis_controller {

#define MOTOR_CONTROLLER_ADDRESS 0x01
#define LEFT 0
#define RIGHT 1
#define FORWARD 0
#define BACKWARD 1

BB8ChassisController::BB8ChassisController(ros::NodeHandle& nh, ros::NodeHandle& nh_private):
    nh_(nh),
    nh_private_(nh_private)
{
    nh_private_.param("motor_command_topic", twistTopic_,
        std::string("/motor_command"));

    nh_private_.param("battery_state_topic", batteryStateTopic_,
        std::string("/battery_state"));

    nh_private_.param("I2C_bus_service_name", I2CBusServiceName_,
        std::string("/i2c_service"));

    nh_private_.param("I2C_message_topic", I2CMessageTopic_,
        std::string("/i2c"));

    batteryStatePublisher_ = nh_.advertise<sensor_msgs::BatteryState>(batteryStateTopic_, 1);
    
    // stuff for controlling actuators
    twistSubscriber_ = nh_.subscribe(twistTopic_, 1,
        &BB8ChassisController::twistCallback, this);
    // I2CClient_ = nh_.serviceClient<raspi_i2c_controller/I2CBusAccess>(I2CBusServiceName_);
    I2CPublisher_ = nh_.advertise<raspi_i2c_buscontroller::I2CMessage>(I2CMessageTopic_, 1);

    // automatically publish battery state
    ros::Timer batteryStateTimer = nh_.createTimer(ros::Duration(10.0),
            &bb8_chassis_controller::BB8ChassisController::publishBatteryStateTimed,
            this, false);
}

void BB8ChassisController::twistCallback(const geometry_msgs::Twist& twistMsg){
    raspi_i2c_buscontroller::I2CMessage I2CMsg;
    I2CMsg.address = MOTOR_CONTROLLER_ADDRESS;
    I2CMsg.mode = raspi_i2c_buscontroller::I2CMessage::WRITE;
    I2CMsg.data.resize(3);

    // calculate motor speeds
    // TODO come up with something better than clamping
    float speedLeft  = clamp(-1.0, 1.0, twistMsg.linear.x - twistMsg.angular.z);
    float speedRight = clamp(-1.0, 1.0, twistMsg.linear.x + twistMsg.angular.z);

    // set dir
    uint8_t dirfield = 0;
    
    if(speedLeft > 0){
        dirfield |= (FORWARD << LEFT);
    }
    else{
        dirfield |= (BACKWARD << LEFT);
    }
    if(speedRight > 0){
        dirfield |= (FORWARD << RIGHT);
    }
    else{
        dirfield |= (BACKWARD << RIGHT);
    }
    I2CMsg.data[0] = dirfield;

    // set speed
    I2CMsg.data[1] = (uint8_t) (255*std::abs(speedLeft)+0.5);
    I2CMsg.data[2] = (uint8_t) (255*std::abs(speedRight)+0.5);

    I2CPublisher_.publish(I2CMsg);
}

void BB8ChassisController::publishBatteryStateTimed(const ros::TimerEvent& timerEvent){
    // ask via I2C bus
    

    // publish to ROS
    sensor_msgs::BatteryState battMsg;
    battMsg.header.stamp = ros::Time::now();
    battMsg.voltage = 12.0;
    battMsg.current = NAN;
    battMsg.charge = NAN;
    battMsg.capacity = NAN;
    battMsg.design_capacity = NAN;
    battMsg.percentage = 1.0;
    battMsg.power_supply_status = sensor_msgs::BatteryState::POWER_SUPPLY_STATUS_UNKNOWN;
    battMsg.power_supply_health = sensor_msgs::BatteryState::POWER_SUPPLY_HEALTH_UNKNOWN;
    // what a shame, there is no POWER_SUPPLY_TECHNOLOGY_PB or something :-(
    battMsg.power_supply_technology = sensor_msgs::BatteryState::POWER_SUPPLY_TECHNOLOGY_UNKNOWN;
    battMsg.present = true;

    batteryStatePublisher_.publish(battMsg);

    return;
}
} //namespace
